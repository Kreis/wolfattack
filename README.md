# Online Room 

## simple room to test online interactions

### dependencies

> sfml in C:/3rdParty/

> add dll to build/debug directory
> add dll to build/release directory

### compile with cmake

```cmake
mkdir build 
cd build

cmake -G "Visual Studio 16 2019" ..

cmake --build . --config Release

Release/onlineroom.exe

```

