#include <iostream>

#include "hz/Hz.h"

#include "GlobalConfig.h"
#include "Game.h"

int main(int argc, char** argv) {

  std::cout << "start online room" << std::endl;

  std::cout << "who are you?" << std::endl;
  std::string username;
  std::cin >> username;
  GlobalConfig::get()->myusername = username;

  // jon
  GlobalConfig::get()->init_pos.push_back(0);
  GlobalConfig::get()->init_pos.push_back(0);

  // ana
  GlobalConfig::get()->init_pos.push_back(32);
  GlobalConfig::get()->init_pos.push_back(0);

  // jair
  GlobalConfig::get()->init_pos.push_back(64);
  GlobalConfig::get()->init_pos.push_back(0);

  Window window;
  window.start("OnlineRoom", 256, 144);

  std::vector<std::string> texturePriority;
  texturePriority.push_back("data/textures/global.png");
  SpriteBatch::get()->init(texturePriority);

  Resources::get()->loadTextureAtlas("data/textures/global.json");

  Game game;
  game.init();

  while (true) {

    window.update();
    if ( ! window.isOpen()) {
      break;
    }

    game.update(window.getElapsedTime());
    game.draw();

    SpriteBatch::get()->flush(&window);
  }


  return 0;
}



