#include "Game.h"

Game::Game() {}
Game::~Game() {}

void Game::init() {
    Rect<int32_t> background_bounds = {0, 0, 256, 144};
    background.init("data/textures/global.json", "field.png", background_bounds);

    std::string name = GlobalConfig::get()->myusername;
    int32_t x, y;
    if (name == "jon") {
        x = GlobalConfig::get()->init_pos[0];
        y = GlobalConfig::get()->init_pos[1];
    }
    if (name == "ana") {
        x = GlobalConfig::get()->init_pos[2];
        y = GlobalConfig::get()->init_pos[3];
    }
    if (name == "jair") {
        x = GlobalConfig::get()->init_pos[4];
        y = GlobalConfig::get()->init_pos[5];
    }
    local_pj.init("data/animators/" + name + ".json", {x, y});

    TopitoClient::get()->init("jFWcXg8cHHXz3iDrGK4TCWKCiNWrqgNbBSdVfnP8XWCvdSycvJiUF40nYFSL");

    std::cout << "to send messages" << std::endl;
    TopitoClient::get()->pushID("SE00");
    TopitoClient::get()->pushID("SU00");
    TopitoClient::get()->pushStr256("coffer-" + GlobalConfig::get()->myusername);
    TopitoClient::get()->pushStr256("aaaa");
    TopitoClient::get()->pushStr256("wwww");
    TopitoClient::get()->pushStr256("rrrr");

    TopitoClient::get()->send();
    std::cout << "done!" << std::endl;

    remotes["jon"].init("jon");
    remotes["ana"].init("ana");
    remotes["jair"].init("jair");

    wolf.init();
}

void Game::update(float deltatime) {
    for (json obj : TopitoClient::get()->retrieve()) {
        std::cout << "\nPretty Printed JSON:\n" << obj.dump(4) << std::endl;

        if (obj.contains("topito-updates")) {
            // update players
            for (const auto& item : obj["topito-updates"]) {
                if (item[1][0] != "users") {
                    continue;
                }
                if (item[0] == "add") {
                    std::cout << "NEW USER" << std::endl;
                    std::string user = item[1][1];
                    if (user != GlobalConfig::get()->myusername) {
                        remotes[user].online = true;
                    }
                }
                if (item[0] == "delete") {
                    std::cout << "OUT USER" << std::endl;
                    std::string user = item[1][1];
                    if (user != GlobalConfig::get()->myusername) {
                        remotes[user].online = false;
                    }
                }
                if (item[0] == "update") {
                    std::string user = item[1][1];
                    std::string key = item[1][2];
                    float val = item[2];

                    if (user == GlobalConfig::get()->myusername) {
                        std::cout << "update local player" << std::endl;
                        if (key == "x") {
                            local_pj.pj.x = val;
                        }
                        if (key == "y") {
                            local_pj.pj.y = val;
                        }
                        if (key == "dir") {
                            local_pj.pj.direction = val;
                        }
                        if (key == "alive") {
                            local_pj.pj.alive = false;
                        }
                    } else {
                        if (key == "alive") {
                            remotes[user].pj.alive = false;
                        } else {
                            remotes[user].handle(key, val);
                        }
                    }
                }
            }
            
            // update wolf
            for (const auto& item : obj["topito-updates"]) {
                if (item[1][0] != "wolf") {
                    continue;
                }
                std::string coord = item[1][1];
                if (coord == "attacking") {
                    std::string user_attacked = item[2];
                    wolf.attacking = user_attacked;
                } else {
                    float value = item[2];
                    wolf.handle(coord, value);
                }
            }
        } else {
            // first one
            for (const auto& item : obj["users"].items()) {
                if (item.key() == GlobalConfig::get()->myusername) {
                    continue;
                }
                remotes[item.key()].online = true;
                remotes[item.key()].moving = item.value()["dir"];
                remotes[item.key()].pj.x = item.value()["x"];
                remotes[item.key()].pj.y = item.value()["y"];
                remotes[item.key()].pj.alive = item.value()["alive"];
            }
            wolf.x = obj["wolf"]["x"];
            wolf.y = obj["wolf"]["y"];
            wolf.goingx = obj["wolf"]["goingx"];
            wolf.goingy = obj["wolf"]["goingy"];
            wolf.attacking = obj["wolf"]["attacking"];
            std::cout << "wolf setted" << std::endl;
        }
    }

    local_pj.update(deltatime);
    for (auto& r : remotes) {
        r.second.update(deltatime);
    }
    wolf.update(deltatime);
}

void Game::draw() {
    SpriteBatch::get()->draw(background.getSprite());

    wolf.draw();
    for (auto& r : remotes) {
        r.second.draw();
    }
    local_pj.draw();
}
