#include "TopitoClient.h"

TopitoClient::TopitoClient() {}
TopitoClient::~TopitoClient() {}

std::unique_ptr<TopitoClient> TopitoClient::instancePtr = nullptr;

TopitoClient* TopitoClient::get() {
  if (instancePtr == nullptr) {
    instancePtr = std::unique_ptr<TopitoClient>(new TopitoClient());
  }

  return instancePtr.get();
}

void TopitoClient::init(std::string token) {
    NetClient::get()->initConnect("data/netinfo.txt");
    for (char c : token) {
        message.push_back(c);
    }
    NetClient::get()->sendMessage(message);
    message.clear();
}

void TopitoClient::pushID(std::string elem) {
    message.push_back(elem[0]);
    message.push_back(elem[1]);
    message.push_back(elem[2]);
    message.push_back(elem[3]);
}

void TopitoClient::pushStr256(std::string elem) {
    if (elem.size() > 255) {
        LOG("Error, str256 higher of 255");
        return;
    }
    message.push_back(elem.size());
    message.insert(message.end(), elem.begin(), elem.end());
}

void TopitoClient::pushNum32(int32_t elem) {
    message.push_back((elem >> 24) & 0xFF);
    message.push_back((elem >> 16) & 0xFF);
    message.push_back((elem >> 8) & 0xFF);
    message.push_back((elem >> 0) & 0xFF);
}

void TopitoClient::send() {
    NetClient::get()->sendMessage(message);
    message.clear();
}

std::vector<json> TopitoClient::retrieve() {
    std::vector<json> result;

    std::vector<std::vector<uint8_t> > received = NetClient::get()->getReceived();
    int32_t id = 0;
    for (std::vector<uint8_t> data : received) {
        std::string jsonString(data.begin(), data.end());
        json j = json::parse(jsonString);

        result.push_back(j);
    }

    return result;
}
