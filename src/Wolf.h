#ifndef _WOLF_H_
#define _WOLF_H_

#include "hz/Hz.h"

class Wolf {
public:
    Wolf();
    ~Wolf();

    void init();
    void update(float deltatime);
    void draw();

    void handle(std::string key, int32_t val);

    Animator* animator;

    float x = 0.0;
    float y = 0.0;

    float goingx = 0.0;
    float goingy = 0.0;

    std::string attacking = "none";

private:
    float speed = 64.0;
};

#endif