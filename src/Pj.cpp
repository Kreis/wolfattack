#include "Pj.h"

Pj::Pj() {}
Pj::~Pj() {}

void Pj::init(std::string animator_path, Vec<int32_t> position) {
    Resources::get()->loadAnimator(animator_path);
    animator = Resources::get()->getAnimator(animator_path);
    animator->runAnimation("walking_down");

    animator->bounds.xy = position;

    this->animator_path = animator_path;
}

void Pj::update(float deltatime) {
    if ( ! alive) {
        return;
    }
    if ( ! walking) {
        setDirection();
        return;
    }
    animator = Resources::get()->getAnimator(this->animator_path);
    
    if (direction == 1) {
        y += deltatime * speed;
    }
    if (direction == 2) {
        y -= deltatime * speed;
    }
    if (direction == 3) {
        x += deltatime * speed;
    }
    if (direction == 4) {
        x -= deltatime * speed;
    }

    animator->update(deltatime);
}

void Pj::draw() {
    animator = Resources::get()->getAnimator(this->animator_path);
    animator->bounds.x = x;
    animator->bounds.y = y;

    if ( ! alive) {
        animator->setColor({0,0,0,255});
    }

    
    animator->draw();
}

void Pj::moveLeft() {
    direction = 4;
    setDirection();
}

void Pj::moveRight() {
    direction = 3;
    setDirection();
}

void Pj::moveDown() {
    direction = 1;
    setDirection();
}

void Pj::moveUp() {
    direction = 2;
    setDirection();
}

void Pj::setDirection() {
    if (direction == 1) {
        animator = Resources::get()->getAnimator(this->animator_path);
        if (walking) {
            animator->runAnimation("walking_down");
        } else {
            animator->runAnimation("idle_down");
        }
        animator->setHorizontalSwitch(false);
        return;
    }
    if (direction == 2) {
        animator = Resources::get()->getAnimator(this->animator_path);
        if (walking) {
            animator->runAnimation("walking_up");
        } else {
            animator->runAnimation("idle_up");
        }
        animator->setHorizontalSwitch(false);
        return;
    }
    if (direction == 3) {
        animator = Resources::get()->getAnimator(this->animator_path);
        if (walking) {
            animator->runAnimation("walking_right");
        } else {
            animator->runAnimation("idle_right");
        }
        animator->setHorizontalSwitch(false);
        return;
    }
    if (direction == 4) {
        animator = Resources::get()->getAnimator(this->animator_path);
        if (walking) {
            animator->runAnimation("walking_right");
        } else {
            animator->runAnimation("idle_right");
        }
        animator->setHorizontalSwitch(true);
        return;
    }
}
