#ifndef _REMOTE_PJ_H_
#define _REMOTE_PJ_H_

#include <iostream>

#include "hz/Hz.h"
#include "Pj.h"

class RemotePj {
public:
    RemotePj();
    ~RemotePj();

    void init(std::string username);
    void update(float deltatime);
    void draw();

    void handle(std::string key, float val);

    Pj pj;
    int32_t moving = 0;
    bool online = false;
};

#endif