#include "LocalPj.h"

LocalPj::LocalPj() {}

LocalPj::~LocalPj() {}

void LocalPj::init(std::string animator_path, Vec<int32_t> position) {
    pj.init(animator_path, position);
}

void LocalPj::update(float deltatime) {

    int32_t last_direction = pj.direction;

    bool walking = false;
    if (InputState::get()->isPressed(KeyButton::Down)) {
        pj.moveDown();
        walking = true;
    } else if (InputState::get()->isPressed(KeyButton::Up)) {
        pj.moveUp();
        walking = true;
    } else if (InputState::get()->isPressed(KeyButton::Right)) {
        pj.moveRight();
        walking = true;
    } else if (InputState::get()->isPressed(KeyButton::Left)) {
        pj.moveLeft();
        walking = true;
    }

    int32_t new_direction = walking ? pj.direction : 0;

    if (new_direction != last_direction) {
        TopitoClient::get()->pushID("U000");
        TopitoClient::get()->pushStr256("coffer-" + GlobalConfig::get()->myusername);
        TopitoClient::get()->pushStr256("wwww");
        TopitoClient::get()->pushNum32(0);

        if (new_direction != 0) {
            TopitoClient::get()->pushID("U000");
            TopitoClient::get()->pushStr256("coffer-" + GlobalConfig::get()->myusername);
            TopitoClient::get()->pushStr256("wwww");
            TopitoClient::get()->pushNum32(new_direction);
        }

        TopitoClient::get()->send();
    }

    pj.walking = walking;
    pj.update(deltatime);
    pj.direction = new_direction;
}

void LocalPj::draw() {
    pj.draw();
}
