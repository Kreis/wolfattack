#ifndef _GAME_H_
#define _GAME_H_

#include <iostream>
#include <map>

#include "hz/Hz.h"

#include "LocalPj.h"
#include "GlobalConfig.h"
#include "TopitoClient.h"
#include "RemotePj.h"
#include "Wolf.h"

class Game {
public:
    Game();
    ~Game();

    void init();

    void update(float deltatime);
    void draw();

    Image background;
    
    LocalPj local_pj;

    std::map<std::string, RemotePj> remotes;

    Wolf wolf;
};

#endif