#include "GlobalConfig.h"

std::unique_ptr<GlobalConfig> GlobalConfig::instancePtr = nullptr;

GlobalConfig::GlobalConfig() {}
GlobalConfig::~GlobalConfig() {}

GlobalConfig* GlobalConfig::get() {
  if (instancePtr == nullptr) {
    instancePtr = std::unique_ptr<GlobalConfig>(new GlobalConfig());
  }

  return instancePtr.get();
}

