#include "RemotePj.h"

RemotePj::RemotePj() {}
RemotePj::~RemotePj() {}

void RemotePj::init(std::string username) {
    pj.init("data/animators/" + username + ".json", {0, 0});
}

void RemotePj::update(float deltatime) {
    if ( ! online) {
        return;
    }
    
    if (moving == 1) {
        pj.moveDown();
    }
    if (moving == 2) {
        pj.moveUp();
    }
    if (moving == 3) {
        pj.moveRight();
    }
    if (moving == 4) {
        pj.moveLeft();
    }
    pj.walking = moving > 0;

    pj.update(deltatime);
}

void RemotePj::draw() {
    if ( ! online) {
        return;
    }
    pj.draw();
}


void RemotePj::handle(std::string key, float val) {
    if (key == "dir") {
        moving = val;
    }
    if (key == "x") {
        pj.x = val;
    }
    if (key == "y") {
        pj.y = val;
    }
}