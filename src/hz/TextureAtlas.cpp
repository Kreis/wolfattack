#include "TextureAtlas.h"

TextureAtlas::TextureAtlas() {
  pTextureRect.resize(60);
}
TextureAtlas::~TextureAtlas() {}

void TextureAtlas::createPiece(std::string name) {
  if (dir[name] != 0) {
    LOG("ERROR: createPiece already use name " + name);
    return;
  }
  index++;
  dir[name] = index;
}

void TextureAtlas::setFrame(std::string name, Rect<int32_t> rect) {

  if (dir[name] == 0) {
    LOG("ERROR: setFrame not Found Piece in texture " + name);
    return;
  }

  int32_t i = dir[name];
  pTextureRect[ i ] = rect;
}

Rect<int32_t> TextureAtlas::getPieceRect(std::string name) {
  if (dir[name] == 0) {
    LOG("ERROR: getPieceRectX not Found Piece in texture " + name);
  }

  return pTextureRect[ dir[name] ];
}


