#ifndef _CLIENT_H_
#define _CLIENT_H_

#include <iostream>
#include <cstring>
#include <memory>

#ifdef __unix__
// sockets
#include <sys/socket.h>
#include <arpa/inet.h>
#include <unistd.h>
// threads
#include <pthread.h>
#endif

#ifdef _WIN32
// sockets
#include <winsock2.h>
#include <ws2tcpip.h>
#pragma comment(lib, "ws2_32.lib")
// threads
#include <Windows.h>
#endif

#include <queue>
#include <mutex>
#include <fstream>
#include <string>

#include "MessageHandler.h"
#include "sharedtypes.h"

class NetClient {
public:
  ~NetClient();

  static NetClient* get();

  bool initConnect(std::string netinfo_path);
  bool sendMessage(std::vector<uint8_t> data);
  void closeConnection();
  std::vector<std::vector<uint8_t> > getReceived();
  
  bool connected;

  void receiveMessages();
private:
  NetClient();
  static std::unique_ptr<NetClient> instancePtr;

#ifdef __unix__
  static void* startThread(void* arg);
#endif
  
  std::string server_address;
  int32_t port;

  std::queue<std::vector<uint8_t> > Q;
  std::mutex mtx;

  void multiplatform_close();
#ifdef _WIN32
  SOCKET client_sock;
#endif
#ifdef __unix__
  int32_t client_sock;
#endif
};


#endif
