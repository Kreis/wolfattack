#include "Animator.h"

Animator::Animator() {}
Animator::~Animator() {}

void Animator::runAnimation(std::string animationName, int32_t sessionId) {

  std::string thisAnimation = getSessionCurrentAnimation(sessionId);

  if (animationName == thisAnimation) {
    return;
  }

  if (animations.find(animationName) == animations.end()) {
    LOG("ERROR: not found animation " + animationName + " in Animator");
    return;
  }

  setSessionCurrentAnimation(sessionId, animationName);
  setSessionCurrentTime(sessionId, 0.0);
  setSessionFrameId(sessionId, 0);
  setSessionStopped(sessionId, false);
}

void Animator::update(float deltatime, int32_t sessionId) {

  std::string thisAnimation = getSessionCurrentAnimation(sessionId);

  if (thisAnimation.length() == 0) {
    LOG("ERROR: no current animation set");
    return;
  }

  if (getSessionStopped(sessionId)) {
    return;
  }

  float thisTime = getSessionCurrentTime(sessionId);
  thisTime += deltatime;
  setSessionCurrentTime(sessionId, thisTime);

  int32_t thisFrameId = getSessionFrameId(sessionId);
  if (thisTime >= delays[ thisAnimation ][ thisFrameId ]) {

    setSessionCurrentTime(sessionId, 0.0);
    setSessionFrameId(sessionId, thisFrameId + 1);

    if (thisFrameId + 1 >= delays[ thisAnimation ].size()) {
      setSessionStopped(sessionId, ! loops[ thisAnimation ]);
      setSessionFrameId(sessionId, stopped ? thisFrameId : 0);
    }
  }
}

void Animator::update(
  float deltatime,
  std::map<std::string, std::string>& status, int32_t sessionId) {
 
  status["stopped"] = getSessionStopped(sessionId) ? "true" : "false";
  std::string newAnimation = graphAnimator.getUpdatedAnimation(status);
  runAnimation(newAnimation, sessionId);
  update(deltatime, sessionId);

}

void Animator::draw(int32_t sessionId) {

  std::string thisAnimation = getSessionCurrentAnimation(sessionId);

  if (thisAnimation.length() == 0) {
    LOG("ERROR: no current animation set");
    return;
  }

  int32_t thisFrameId = getSessionFrameId(sessionId);
  Rect<int32_t>& rect = animations[ thisAnimation ][ thisFrameId ];

  sprite.textureName = textureName;
  sprite.bounds = bounds;
  sprite.textureRect = rect;
  sprite.horizontalSwitch = getSessionFlipHorizontal(sessionId);
  sprite.verticalSwitch = getSessionFlipVertical(sessionId);
  sprite.rotation = getSessionRotation(sessionId);
  SpriteBatch::get()->draw(&sprite);
}

void Animator::setHorizontalSwitch(bool v, int32_t sessionId) {
  setSessionFlipHorizontal(sessionId, v);
}

void Animator::setVerticalSwitch(bool v, int32_t sessionId) {
  setSessionFlipVertical(sessionId, v);
}

void Animator::setRotation(float v, int32_t sessionId) {
  setSessionRotation(sessionId, v);
}

void Animator::setColor(Rect<int32_t> color) {
  sprite.color = color;
}

int32_t Animator::getNewSession() {
  int32_t id;
  if (queSessions.size() > 0) {
    id = queSessions.front();
    queSessions.pop();
  } else {
    id = intSession++;
  }

  sessionCurrentAnimation[ id ] = "";
  sessionCurrentTime[ id ] = 0.0;
  sessionFrameId[ id ] = 0;
  sessionStopped[ id ] = false;
  sessionFlipHorizontal[ id ] = false;
  sessionFlipVertical[ id ] = false;
  sessionRotation[ id ] = 0.0;

  return id;
}

std::string Animator::getCurrentAnimation(int32_t sessionId) {
  return getSessionCurrentAnimation(sessionId);
}

bool Animator::getStopped(int32_t sessionId) {
  return getSessionStopped(sessionId);
}

std::string Animator::getSessionCurrentAnimation(int32_t id) {
  if (id == -1) {
    return currentAnimation;
  }
  return sessionCurrentAnimation[ id ];
}

float Animator::getSessionCurrentTime(int32_t id) {
  if (id == -1) {
    return currentTime;
  }
  return sessionCurrentTime[ id ];
}

int32_t Animator::getSessionFrameId(int32_t id) {
  if (id == -1) {
    return frameId;
  }
  return sessionFrameId[ id ];
}

bool Animator::getSessionStopped(int32_t id) {
  if (id == -1) {
    return stopped;
  }
  return sessionStopped[ id ];
}

bool Animator::getSessionFlipHorizontal(int32_t id) {
  if (id == -1) {
    return flipHorizontal;
  }
  return sessionFlipHorizontal[ id ];
}

bool Animator::getSessionFlipVertical(int32_t id) {
  if (id == -1) {
    return flipVertical;
  }
  return sessionFlipVertical[ id ];
}

float Animator::getSessionRotation(int32_t id) {
  if (id == -1) {
    return rotation;
  }
  return sessionRotation[ id ];
}
void Animator::setSessionCurrentAnimation(int32_t id, std::string v) {
  if (id == -1) {
    currentAnimation = v;
    return;
  }
  sessionCurrentAnimation[ id ] = v;
}

void Animator::setSessionCurrentTime(int32_t id, float v) {
  if (id == -1) {
    currentTime = v;
    return;
  }
  sessionCurrentTime[ id ] = v;
}

void Animator::setSessionFrameId(int32_t id, int32_t v) {
  if (id == -1) {
    frameId = v;
    return;
  }
  sessionFrameId[ id ] = v;
}

void Animator::setSessionStopped(int32_t id, bool v) {
  if (id == -1) {
    stopped = v;
    return;
  }
  sessionStopped[ id ] = v;
}

void Animator::setSessionFlipHorizontal(int32_t id, bool v) {
  if (id == -1) {
    flipHorizontal = v;
    return;
  }
  sessionFlipHorizontal[ id ] = v;
}

void Animator::setSessionFlipVertical(int32_t id, bool v) {
  if (id == -1) {
    flipVertical = v;
    return;
  }
  sessionFlipVertical[ id ] = v;
}

void Animator::setSessionRotation(int32_t id, float v) {
  if (id == -1) {
    rotation = v;
    return;
  }
  sessionRotation[ id ] = v;
}



