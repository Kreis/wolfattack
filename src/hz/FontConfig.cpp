#include "FontConfig.h"

FontConfig::FontConfig() {}

bool FontConfig::validate() {
  validated =
    this->letters.size() > 0 &&
    this->map_kerning.size() > 0 &&
    this->line_height > 0;

  return validated;
}

bool FontConfig::isValidated() const {
  return validated;
}

void FontConfig::setLineHeight(uint32_t line_height) {
  this->line_height = line_height;
}

void FontConfig::setSpaceWidth(uint32_t space_width) {
  this->space_width = space_width;
}

void FontConfig::setKernings(
  const std::vector<uint8_t>& firsts,
  const std::vector<uint8_t>& seconds,
  const std::vector<int8_t>& amounts) {

  assert(firsts.size() == seconds.size());
  assert(seconds.size() == amounts.size());
  // All sizes are the same

  map_kerning.clear();
  for (uint32_t i = 0; i < firsts.size(); i++) {
    map_kerning[ firsts[ i ] ][ seconds[ i ] ] = amounts[ i ];
  }
}

void FontConfig::setLetters(
  const std::vector<uint8_t>& ascii,
  const std::vector<Rect<int32_t> >& texture_area,
  const std::vector<Vec<int32_t> >& offset_position,
  const std::vector<uint32_t>& xadvance,
  std::string textures_name) {

  assert(ascii.size() == texture_area.size());
  assert(texture_area.size() == offset_position.size());
  assert(offset_position.size() == xadvance.size());
  assert(textures_name.length() > 0);
  // All sizes are the same
  letters.clear();
  for (uint32_t i = 0; i < ascii.size(); i++) {
    letters.push_back(Letter(
      ascii[ i ],
      texture_area[ i ].x,
      texture_area[ i ].y,
      texture_area[ i ].w,
      texture_area[ i ].h,
      offset_position[ i ].x,
      offset_position[ i ].y,
      xadvance[ i ],
      textures_name));
    this->map_letter_index[ ascii[ i ] ] = i;
  }
}

uint32_t FontConfig::getLineHeight() const {
  return this->line_height;
}

uint32_t FontConfig::getSpaceWidth() const {
  return space_width;
}

float FontConfig::getLetterDelay() const {
  return letter_delay;
}

const Letter* FontConfig::getLetter(uint8_t ascii) const {
  std::map<uint8_t, uint32_t>::const_iterator itr =
    this->map_letter_index.find(ascii);
  if (itr == this->map_letter_index.end()) {
    return nullptr;
  }

  return &this->letters[ itr->second ];
}

int8_t FontConfig::getKerning(uint8_t a, uint8_t b) const {
  std::map<uint8_t, std::map<uint8_t, int8_t> >::const_iterator ia =
    this->map_kerning.find(a);
  if (ia == this->map_kerning.end()) {
      return 0;
  }
  std::map<uint8_t, int8_t>::const_iterator ib =
    ia->second.find(b);
  if (ib == ia->second.end()) {
      return 0;
  }

  return this->map_kerning.at(a).at(b);
}





