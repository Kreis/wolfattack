#include "NetClient.h"

NetClient::NetClient() {
  connected = false;
}
NetClient::~NetClient() {}

std::unique_ptr<NetClient> NetClient::instancePtr = nullptr;

NetClient* NetClient::get() {
  if (instancePtr == nullptr) {
    instancePtr = std::unique_ptr<NetClient>(new NetClient());
  }

  return instancePtr.get();
}

#ifdef __unix__
void* NetClient::startThread(void* arg) {
  NetClient* client = static_cast<NetClient*>(arg);
  client->receiveMessages();
  return nullptr;
}
#endif
#ifdef _WIN32
DWORD WINAPI startThread(LPVOID arg) {
  NetClient* client = static_cast<NetClient*>(arg);
  client->receiveMessages();
  return 0;
}
#endif

void NetClient::receiveMessages() {
 
  MessageHandler message_handler;
  while (true) {
    char buffer[1024] = {0};
    int bytes_received = recv(client_sock, buffer, sizeof(buffer), 0);
    if (bytes_received == -1) {
        LOG("Error: at receiving server message");
        multiplatform_close();
        connected = false;
        break;
    } else if (bytes_received == 0) {
        LOG("Connection closed by server");
        multiplatform_close();
        connected = false;
        break;
    }

    message_handler.push(buffer, bytes_received);
    while ( ! message_handler.q.empty()) {
      LOG("arrive a valid message");
      std::vector<uint8_t> data = message_handler.q.front();
      message_handler.q.pop();

      // sync code
      {
        std::lock_guard<std::mutex> guard(mtx);
        Q.push(data);
      }
    }
  }
  
  multiplatform_close();
#ifdef __unix__
  pthread_exit(NULL);
#endif
#ifdef _WIN32
  ExitThread(0);
#endif
}

bool NetClient::initConnect(std::string netinfo_path) {
  std::ifstream myfile(netinfo_path);
  if ( ! myfile.is_open()) {
    LOG("Error: " + netinfo_path + " not found");
  }
  if ( ! myfile.good()) {
    LOG("Error: " + netinfo_path + " not good");
  }
  myfile >> server_address;
  myfile >> port;

  LOG("NetClient starting with " + server_address +
    " and port " + std::to_string(port));

#ifdef _WIN32
  WSADATA wsaData;
  if (WSAStartup(MAKEWORD(2, 2), &wsaData) != 0) {
      return false;
  }

  client_sock = socket(AF_INET, SOCK_STREAM, 0);
  if (client_sock == INVALID_SOCKET) {
      WSACleanup();
      return false;
  }
#endif
#ifdef __unix__
  client_sock = socket(AF_INET, SOCK_STREAM, 0);
  if (client_sock == -1) {
      LOG("Error: creating client socket");
      return false;
  }
#endif

  sockaddr_in server_addr;
  memset(&server_addr, 0, sizeof(server_addr));
  server_addr.sin_family = AF_INET;
  server_addr.sin_port = htons(port);

  if (inet_pton(AF_INET, server_address.c_str(), &server_addr.sin_addr) == -1) {
    LOG("Error: server address not valid");
    multiplatform_close();
    return false;
  }

  int32_t trySockFun =
    connect(client_sock, (sockaddr*)&server_addr, sizeof(server_addr));
  if (trySockFun == -1) {
    LOG("Error: connecting to the server");
    multiplatform_close();
    return false;
  }

  LOG("Connection stablished with server");

#ifdef __unix__
  pthread_t thread;
  int thread_status =
    pthread_create(&thread, NULL, &NetClient::startThread, this);
  if (thread_status != 0) {
      LOG("Error: creating thread to receive messages");
      return false;
  }
#endif
#ifdef _WIN32
  HANDLE hThread = CreateThread(NULL, 0, startThread, this, 0, NULL);
  if (hThread == NULL) {
    LOG("Error: creating thread to receive messages");
    return false;
  }
  CloseHandle(hThread);
#endif

  connected = true;
  return true;
}

bool NetClient::sendMessage(std::vector<uint8_t> data) {

  if ( ! connected) {
    LOG("WARNING: send to net not working is Offline");
    return false;
  }

  if (send(client_sock, (char*)&data[0], data.size(), 0) == -1) {
    LOG("Error: sending message to the server");
    return false; 
  }

  return true;
}

void NetClient::closeConnection() {
  multiplatform_close();
}

std::vector<std::vector<uint8_t> > NetClient::getReceived() {

  // sync code
  std::vector<std::vector<uint8_t> > data;
  {
    std::lock_guard<std::mutex> guard(mtx);
    while ( ! Q.empty()) {
      std::vector<uint8_t> data_single = Q.front();
      data.push_back(data_single);
      Q.pop();
    }
  }

  return data;
}

void NetClient::multiplatform_close() {
#ifdef _WIN32
  closesocket(client_sock);
  WSACleanup();
#endif
#ifdef __unix__
  close(client_sock);
#endif
}


