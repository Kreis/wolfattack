#include "Camera.h"

Camera::Camera() {}
Camera::~Camera() {}

std::unique_ptr<Camera> Camera::instancePtr = nullptr;

Camera* Camera::get() {
  if (instancePtr == nullptr) {
    instancePtr = std::unique_ptr<Camera>(new Camera());
  }

  return instancePtr.get();
}

