#ifndef _SHARED_TYPES_H_
#define _SHARED_TYPES_H_

#ifdef _MSC_VER
#if _MSC_VER >= 1600
#include <cstdint>
#else
typedef __int8              int8_t;
typedef __int16             int16_t;
typedef __int32             int32_t;
typedef __int64             int64_t;
typedef unsigned __int8     uint8_t;
typedef unsigned __int16    uint16_t;
typedef unsigned __int32    uint32_t;
typedef unsigned __int64    uint64_t;
#endif
#elif __GNUC__ >= 3
#include <cstdint>
#endif
#include <assert.h>
#include <iostream>
#include <filesystem>

#define LOG(message) \
    std::cout << "File: " << \
    std::filesystem::path(__FILE__).filename().string() << \
    ", Line: " << __LINE__ << " - " << \
    message << std::endl

template <class T>
class Vec {
public:
  T x = 0;
  T y = 0;

  Vec<T>() {}

  Vec<T>(T x, T y) {
    this->x = x;
    this->y = y;
  }

  template<typename U>
  operator Vec<U>() const {
    Vec<U> newone;
    newone.x = static_cast<U>(x);
    newone.y = static_cast<U>(y);
    return newone;
  }
  bool operator == (const Vec<T>& other) const {
    return this->x == other.x && this->y == other.y;
  }
  bool operator != (const Vec<T>& other) const {
    return ! (*this == other);
  }
  Vec<T> operator + (T v) const {
    Vec<T> n;
    n.x = this->x + v;
    n.y = this->y + v;
    return n;
  }
  Vec<T> operator + (const Vec<T>& other) const {
    Vec<T> n;
    n.x = this->x + other.x;
    n.y = this->y + other.y;
    return n;
  }
  Vec<T> operator - (T v) const {
    Vec<T> n;
    n.x = this->x - v;
    n.y = this->y - v;
    return n;
  }
  Vec<T> operator - (const Vec<T>& other) const {
    Vec<T> n;
    n.x = this->x - other.x;
    n.y = this->y - other.y;
    return n;
  }
  Vec<T> operator * (T v) const {
    Vec<T> n;
    n.x = this->x * v;
    n.y = this->y * v;
    return n;
  }
  Vec<T> operator * (const Vec<T>& other) const {
    Vec<T> n;
    n.x = this->x * other.x;
    n.y = this->y * other.y;
    return n;
  }
  Vec<T> operator / (T v) const {
    Vec<T> n;
    n.x = this->x / v;
    n.y = this->y / v;
    return n;
  }
  Vec<T> operator / (const Vec<T>& other) const {
    Vec<T> n;
    n.x = this->x / other.x;
    n.y = this->y / other.y;
    return n;
  }
  Vec<T> operator = (T v) {
    this->x = v;
    this->y = v;
    return *this;
  }
  Vec<T> operator = (const Vec<T>& other) {
    this->x = other.x;
    this->y = other.y;
    return *this;
  }

  friend std::ostream& operator << (std::ostream& out, const Vec<T>& me) {
    out << "x: " << me.x << " y: " << me.y << std::endl;
    return out;
  }

  inline float distance(const Vec<T>& other) {
    float diffx = other.x - x;
    float diffy = other.y - y;
    return sqrt(diffx*diffx + diffy*diffy);
  }

  inline Vec<T> normalized() {
    T dis = sqrt(x*x + y*y);
    if (dis == 0) {
      return { 0, 0 };
    }
    return { x / dis, y / dis };
  }
};

template <class T>
class Rect {
friend class Rect;
public:
  Rect() {}
  Rect(T _x, T _y, T _w, T _h) {
    data.single[ 0 ] = _x;
    data.single[ 1 ] = _y;
    data.single[ 2 ] = _w;
    data.single[ 3 ] = _h;
  }
  Rect(const Rect<T>& other) {
    data.single[ 0 ] = other.x;
    data.single[ 1 ] = other.y;
    data.single[ 2 ] = other.w;
    data.single[ 3 ] = other.h;
  }

  T& x = data.single[ 0 ];
  T& y = data.single[ 1 ];
  T& w = data.single[ 2 ];
  T& h = data.single[ 3 ];

  T& r = data.single[ 0 ];
  T& g = data.single[ 1 ];
  T& b = data.single[ 2 ];
  T& a = data.single[ 3 ];

  Vec<T>& xy = data.vec[ 0 ];
  Vec<T>& wh = data.vec[ 1 ];

  template<typename U>
  operator Rect<U>() const {
    Rect<U> newone;
    newone.data.single[ 0 ] = static_cast<U>(x);
    newone.data.single[ 1 ] = static_cast<U>(y);
    newone.data.single[ 2 ] = static_cast<U>(w);
    newone.data.single[ 3 ] = static_cast<U>(h);
    return newone;
  }
  Rect<T> operator = (const Rect<T>& other) {
    data.single[ 0 ] = other.x;
    data.single[ 1 ] = other.y;
    data.single[ 2 ] = other.w;
    data.single[ 3 ] = other.h;
    return *this;
  }
  bool operator == (const Rect<T>& other) const {
    return this->x == other.x && this->y == other.y
      && this->w == other.w && this->h == other.h;
  }
  bool operator != (const Rect<T>& other) const {
    return ! (*this == other);
  }
  friend std::ostream& operator << (std::ostream& out, Rect<T>& me) {
    out << "x: " << me.x << " y: " << me.y << " w: "
      << me.w << " h: " << me.h << std::endl;
    return out;
  }

  inline bool collide(const Rect<T>& other) {
    return ! (this->x >= other.x + other.w ||
      this->x + this->w <= other.x ||
      this->y >= other.y + other.h ||
      this->y + this->h <= other.y);
  }

  inline bool contains(const Vec<T>& v) const {
    return v.x >= this->x &&
      v.x <= this->x + this->w &&
      v.y >= this->y &&
      v.y <= this->y + this->h;
  }

  inline bool contains(const Rect<T>& other) const {
    return other.x >= x && other.x + other.w <= x + w
      && other.y >= y && other.y + other.h <= y + h;
  }

private:
  union Data {
    T single[ 4 ];
    Vec<T> vec[ 2 ];
  };
  Data data = { 0, 0, 0, 0 };
};

class vf {
public:
  vf() : n(0), d(1) {}
  vf(float v) : n(0), d(1) { fromFloat(v); }
  vf(const vf& v) : n(v.n), d(v.d) { simplify(); }
  vf(int64_t n, int64_t d) : n(n), d(d) { simplify(); }
  ~vf() {}
  float get() const {
    return (float)n / (float)d;
  }

  // aritmetic
  vf operator - () const {
    return vf(n * -1, d);
  }
  friend vf operator - (float other, const vf& me) {
    return vf(other) - me;
  }
  vf operator - (float other) const {
    return (*this) - vf(other);
  }
  vf operator - (const vf& other) const {
    const int64_t l = getLcm(d, other.d);
    return vf(n * (l / d) - (other.n * (l / other.d)), l);
  }
  friend vf operator + (float other, const vf& me) {
    return vf(other) + me;
  }
  vf operator + (float other) const {
    return (*this) + vf(other);
  }
  vf operator + (const vf& other) const {
    const int64_t lcm = getLcm(d, other.d);
    return vf(n * (lcm / d) + (other.n * (lcm / other.d)), lcm);
  }
  friend vf operator / (float other, const vf& me) {
    return vf(other) / me;
  }
  vf operator / (float other) const {
    return (*this) / vf(other);
  }
  vf operator / (const vf& other) const {
    return vf(n * other.d, d * other.n);
  }
  friend vf operator * (float other, vf& me) {
    return vf(other) * me;
  }
  vf operator * (float other) {
    return (*this) * vf(other);
  }
  vf operator * (const vf& other) {
    return vf(n * other.n, d * other.d);
  }
  void operator -= (float other) {
    (*this) -= vf(other);
  }
  void operator -= (const vf& other) {
    const int64_t lcm = getLcm(d, other.d);
    n = (n * (lcm / d)) - (other.n * (lcm / other.d));
    d = lcm;
  }
  void operator += (float other) {
    (*this) += vf(other);
  }
  void operator += (const vf& other) {
    const int64_t lcm = getLcm(d, other.d);
    n = (n * (lcm / d)) + (other.n * (lcm / other.d));
    d = lcm;
  }
  void operator /= (float other) {
    (*this) /= vf(other);
  }
  void operator /= (const vf& other) {
    n *= other.d;
    d *= other.n;
    simplify();
  }
  void operator *= (float other) {
    (*this) *= vf(other);
  }
  void operator *= (const vf& other) {
    n *= other.n;
    d *= other.d;
    simplify();
  }

  // boolean
  friend bool operator == (float other, const vf& me) {
    return vf(other) == me;
  }
  bool operator == (float other) const {
    return (*this) == vf(other);
  }
  bool operator == (const vf& other) const {
    return n == other.n && d == other.d;
  }
  friend bool operator != (float other, const vf& me) {
    return vf(other) != me;
  }
  bool operator != (float other) const {
    return (*this) != vf(other);
  }
  bool operator != (const vf& other) const {
    return n != other.n || d != other.d;
  }
  friend bool operator < (float other, const vf& me) {
    return vf(other) < me;
  }
  bool operator < (float other) const {
    return (*this) < vf(other);
  }
  bool operator < (const vf& other) const {
    const int64_t lcm = getLcm(d, other.d);
    return n * (lcm / d) < other.n * (lcm / other.d);
  }
  friend bool operator > (float other, const vf& me) {
    return vf(other) > me;
  }
  bool operator > (float other) const {
    return (*this) > vf(other);
  }
  bool operator > (const vf& other) const {
    const int64_t lcm = getLcm(d, other.d);
    return n * (lcm / d) > other.n * (lcm / other.d);
  }
  friend bool operator >= (float other, const vf& me) {
    return vf(other) >= me;
  }
  bool operator >= (float other) const {
    return (*this) >= vf(other);
  }
  bool operator >= (const vf& other) const {
    const int64_t lcm = getLcm(d, other.d);
    return n * (lcm / d) >= other.n * (lcm / other.d);
  }
  friend bool operator <= (float other, const vf& me) {
    return vf(other) <= me;
  }
  bool operator <= (float other) const {
    return (*this) <= vf(other);
  }
  bool operator <= (const vf& other) const {
    const int64_t lcm = getLcm(d, other.d);
    return n * (lcm / d) <= other.n * (lcm / other.d);
  }
  friend std::ostream& operator <<(std::ostream& os, const vf& v) {
    os << v.n << " / " << v.d << " = " << v.get();
    return os;
  }

private:
  int64_t n;
  int64_t d;
  void fromFloat(float v) {
    n = (int64_t)v;
    uint8_t cont = 0;
    while (cont < 4 && v - (float)n != 0) {
      cont++;
      v *= 10;
      n = (int64_t)v;
    }
    while (cont-- > 0) {
      d *= 10;
    }
    simplify();
  }

  inline int64_t getGcd(int64_t a, int64_t b) const {
    if (b == 0) return a;
    return getGcd(b, a % b);
  }

  inline int64_t getLcm(int64_t a, int64_t b) const {
    a = a < 0 ? -a : a;
    b = b < 0 ? -b : b;
    return (a * b) / getGcd(a < 0 ? -a : a, b < 0 ? -b : b);
  }

  inline void simplify() {
    const int64_t gcd = getGcd(n, d);
    this->n = n / gcd;
    this->d = d / gcd;
    n = d < 0 ? -n : n;
    d = d < 0 ? -d : d;
  }
};

#endif
