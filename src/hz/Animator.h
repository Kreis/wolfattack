#ifndef _ANIMATOR_H_
#define _ANIMATOR_H_

#include <iostream>
#include <map>
#include <queue>

#include "sharedtypes.h"
#include "TextureAtlas.h"
#include "SpriteBatch.h"
#include "GraphAnimator.h"

class Animator {
friend class Resources;
public:
  ~Animator();

  void runAnimation(std::string animationName, int32_t sessionId = -1);
  void update(float deltatime, int32_t sessionId = -1);
  void update(float deltatime,
    std::map<std::string, std::string>& status,
    int32_t sessionId = -1);
  void draw(int32_t sessionId = -1);
  void setHorizontalSwitch(bool v, int32_t sessionId = -1);
  void setVerticalSwitch(bool v, int32_t sessionId = -1);
  void setRotation(float v, int32_t sessionId = -1);
  
  int32_t getNewSession();

  std::string textureName;
  Rect<int32_t> bounds;
  void setColor(Rect<int32_t> color);

  std::string getCurrentAnimation(int32_t sessionId = -1);
  bool getStopped(int32_t sessionId = -1);

private:
  Animator();
  Sprite sprite;

  std::map<std::string, std::vector<Rect<int32_t> > > animations;
  std::map<std::string, std::vector<float> > delays;
  std::map<std::string, bool> loops;
  float currentTime = 0;

  std::string currentAnimation = "";
  int32_t frameId = 0;
  bool stopped = false;
  bool flipHorizontal = false;
  bool flipVertical = false;
  float rotation = 0;

  // sessions
  // get
  std::string getSessionCurrentAnimation(int32_t id);
  float getSessionCurrentTime(int32_t id);
  int32_t getSessionFrameId(int32_t id);
  bool getSessionStopped(int32_t id);
  bool getSessionFlipHorizontal(int32_t id);
  bool getSessionFlipVertical(int32_t id);
  float getSessionRotation(int32_t id);
  // set
  void setSessionCurrentAnimation(int32_t id, std::string v);
  void setSessionCurrentTime(int32_t id, float v);
  void setSessionFrameId(int32_t id, int32_t v);
  void setSessionStopped(int32_t id, bool v);
  void setSessionFlipHorizontal(int32_t id, bool v);
  void setSessionFlipVertical(int32_t id, bool v);
  void setSessionRotation(int32_t id, float v);
  
  int32_t intSession;
  std::queue<int32_t> queSessions;
  std::map<int32_t, std::string> sessionCurrentAnimation;
  std::map<int32_t, float> sessionCurrentTime;
  std::map<int32_t, int32_t> sessionFrameId;
  std::map<int32_t, bool> sessionStopped;
  std::map<int32_t, bool> sessionFlipHorizontal;
  std::map<int32_t, bool> sessionFlipVertical;
  std::map<int32_t, float> sessionRotation;

  GraphAnimator graphAnimator;
};


#endif
