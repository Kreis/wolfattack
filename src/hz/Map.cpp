#include "Map.h"

Map::Map() {}
Map::~Map() {}

std::vector<sf::Vertex>* Map::getVecVertex() {
  LOG("Map generating vecVertex");

  if (vecVertex.size() > 0) {
    return &vecVertex;
  }

  for (int32_t i = 0; i < width; i++) {
    for (int32_t j = 0; j < height; j++) {
      
      int32_t id = grid[ j * width + i ];
      if (id == 0) {
        continue;
      }

      id -= 1;

      int32_t x = id % tilesetColumns;
      int32_t y = id / tilesetColumns;
      
      int32_t len = vecVertex.size();
      vecVertex.push_back(sf::Vertex());
      vecVertex.push_back(sf::Vertex());
      vecVertex.push_back(sf::Vertex());
      vecVertex.push_back(sf::Vertex());

      vecVertex[ len + 0 ].position.x = i * tilewidth;
      vecVertex[ len + 0 ].position.y = j * tileheight;
      vecVertex[ len + 0 ].texCoords.x = x * tilewidth;
      vecVertex[ len + 0 ].texCoords.y = y * tileheight;
      
      vecVertex[ len + 1 ].position.x = i * tilewidth + tilewidth;
      vecVertex[ len + 1 ].position.y = j * tileheight;
      vecVertex[ len + 1 ].texCoords.x = x * tilewidth + tilewidth;
      vecVertex[ len + 1 ].texCoords.y = y * tileheight;

      vecVertex[ len + 2 ].position.x = i * tilewidth + tilewidth;
      vecVertex[ len + 2 ].position.y = j * tileheight + tileheight;
      vecVertex[ len + 2 ].texCoords.x = x * tilewidth + tilewidth;
      vecVertex[ len + 2 ].texCoords.y = y * tileheight + tileheight;

      vecVertex[ len + 3 ].position.x = i * tilewidth;
      vecVertex[ len + 3 ].position.y = j * tileheight + tileheight;
      vecVertex[ len + 3 ].texCoords.x = x * tilewidth;
      vecVertex[ len + 3 ].texCoords.y = y * tileheight + tileheight;
    }
  }

  return &vecVertex;
}





