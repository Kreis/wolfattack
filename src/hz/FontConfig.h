#ifndef _FONT_CONFIG_H_
#define _FONT_CONFIG_H_

#include <memory>
#include <map>
#include <vector>
#include <assert.h>

#include "sharedtypes.h"
#include "Letter.h"

class FontConfig {
public:
    FontConfig();

    bool validate();
    bool isValidated() const;

    // size of the row height
    void setLineHeight(uint32_t line_height);

    // space is handled independently
    // (not as character)
    void setSpaceWidth(uint32_t space_width);

    void setKernings(
      const std::vector<uint8_t>& firsts,
	  	const std::vector<uint8_t>& seconds,
	  	const std::vector<int8_t>& amounts);
    void setLetters(
      const std::vector<uint8_t>& ascii,
      const std::vector<Rect<int32_t> >& texture_area,
      const std::vector<Vec<int32_t> >& offset_position,
      const std::vector<uint32_t>& xadvance,
      std::string textures_name);

    uint32_t getLineHeight() const;
    uint32_t getSpaceWidth() const;
    float getLetterDelay() const;
    const Letter* getLetter(uint8_t ascii) const;
    int8_t getKerning(uint8_t a, uint8_t b) const;
	
private:
    std::vector<Letter> letters;
    std::map<uint8_t, std::map<uint8_t, int8_t> > map_kerning;
    std::map<uint8_t, uint32_t> map_letter_index;
    uint32_t line_height = 0;
    uint32_t space_width = 0;
    float letter_delay = 0.0f;
    bool validated = false;
};

#endif
