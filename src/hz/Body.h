#ifndef _BODY_H_
#define _BODY_H_

#include "sharedtypes.h"

class Body {
friend class Physics;
public:
	~Body();

	vf getX() const;
	vf getY() const;
	vf getWidth() const;
	vf getHeight() const;

  bool isEnabled() const;

	void setPosition(float x, float y);
	void setPositionX(float x);
	void setPositionY(float y);
	void setType(int32_t v);
  void setEnabled(bool v);

	void move(vf x, vf y);

	void moveTo(vf x, vf y);
	void moveToX(vf x);
	void moveToY(vf y);

	bool isColliding() const;
	void off();

  bool collidingLeft;
  bool collidingRight;
  bool collidingUp;
  bool collidingDown;

private:
	Body();
	void init(vf x, vf y, vf width, vf height, bool isDynamic);
	vf x;
	vf y;
	vf width;
	vf height;
	Vec<vf> tryingMove;
	
	bool slopePriority;
	bool isDynamic;
	bool alive; // inactive
	bool buried; // ready to be taken as a new hbody
	bool colliding;
  bool enabled;

	int32_t type = 0;
};

#endif
