#include "Resources.h"

using json = nlohmann::json;

Resources::Resources() {}
Resources::~Resources() {}

std::unique_ptr<Resources> Resources::instancePtr = nullptr;

Resources* Resources::get() {

  if (instancePtr == nullptr) {
    
    instancePtr = std::unique_ptr<Resources>(new Resources());

  }

  return instancePtr.get();
}

void Resources::loadTexture(std::string name) {

  if (textures.find(name) != textures.end()) {
    LOG("WARNING: Texture already exists");
    return;
  }

  LOG("load texture: " + name);

  sf::Texture texture;
  if ( ! texture.loadFromFile(name) ) {
    LOG("ERROR: Not found texture in path " + name);
  }

  textures[ name ] = texture;
}

sf::Texture* Resources::getTexture(std::string name) {

  if (textures.find(name) == textures.end()) {
    LOG("ERROR: Texture not loaded with name " + name);
    return nullptr;
  }

  return &textures[ name ];
}


void Resources::loadTextureAtlas(std::string name) {

  if (texturesAtlas[name] != nullptr) {
    LOG("WARNING: texture atlas " + name + " already loaded");
    return;
  }

  LOG("load texture atlas: " + name);

  std::ifstream f(name);
  if ( ! f) {
    LOG("ERROR: not found " + name);
    return;
  }

  if ( ! json::accept(f)) {
    LOG("ERROR: not valid json: " + name);
  }

  f.seekg(0);

  json data = json::parse(f);

  std::string texturePath = std::string(data.at("meta").at("image"));

  std::unique_ptr<TextureAtlas> textureAtlas =
    std::unique_ptr<TextureAtlas>(new TextureAtlas());

  loadTexture(texturePath);
  textureAtlas->textureName = texturePath;
  
  for (auto& i : data.at("frames").items()) {

    textureAtlas->createPiece(i.key());
    
    auto& frame = i.value().at("frame");
    Rect<int32_t> rect;
    rect.x = frame.at("x");
    rect.y = frame.at("y");
    rect.w = frame.at("w");
    rect.h = frame.at("h");
    textureAtlas->setFrame(i.key(), rect);
  }

  texturesAtlas[name] = std::move(textureAtlas);
}

TextureAtlas* Resources::getTextureAtlas(std::string name) {

  if (texturesAtlas[name] == nullptr) {
    LOG("ERROR: texture atlas not found with name " + name);
    return nullptr;
  }

  return texturesAtlas[name].get();
}

void Resources::loadAnimator(std::string name) {

  if (animators[ name ] != nullptr) {
    LOG("WARNING: animator " + name + " already loaded");
    return;
  } 

  LOG("load animator: " + name);

  std::ifstream f(name);
  if ( ! f) {
    LOG("ERROR: not found " + name);
    return;
  }

  if ( ! json::accept(f)) {
    LOG("ERROR: not valid json: " + name);
  }

  f.seekg(0);

  json data = json::parse(f);

  std::unique_ptr<Animator> animator =
    std::unique_ptr<Animator>(new Animator());

  animator->bounds.w = data.at("width");
  animator->bounds.h = data.at("height");

  std::string textureAtlasName = data.at("textureAtlas");
  
  TextureAtlas* textureAtlas =
    Resources::get()->getTextureAtlas(textureAtlasName);
 
  animator->textureName = textureAtlas->textureName; 

  for (auto& i : data.at("animations").items()) {
    
    // Initialize an animation
    animator->animations[ i.key() ] = std::vector<Rect<int32_t> >();
    animator->delays[ i.key() ] = std::vector<float>();
    animator->loops[ i.key() ] = i.value().at("loop");

    // each frame for animation
    for (auto& j : i.value().at("frames")) {

      animator->animations[ i.key() ].push_back(
        textureAtlas->getPieceRect(j.at("piece")));
      
      Rect<int32_t> cc;
      cc.x = 255;
      cc.y = 255;
      cc.w = 255;
      cc.h = 255;

      animator->delays[ i.key() ].push_back(
        j.at("delay"));
    }
  }

  // if graph
  if (data.contains("graph")) {
    LOG("init graph in animator");

    std::map<std::string, int32_t> mappingIds;
    int32_t mId = 0;
    for (auto& node : data.at("graph")) {
      mappingIds[ node.at("animation") ] = mId++;
    }

    for (auto& node : data.at("graph")) {

      int gId = animator->graphAnimator.nodes.size();
      animator->graphAnimator.nodes.push_back(NodeAnimator());
      animator->graphAnimator.visited.push_back(false);

      NodeAnimator& nA = animator->graphAnimator.nodes[ gId ];
      nA.animationName = node.at("animation");
      // set default
      if (node.contains("default") && node.at("default")) {
        animator->graphAnimator.nodeId = gId;
      }

      // check pointing nodes
      for (auto& to : node.at("to")) {
        nA.nodeAnimators.push_back(mappingIds[ to.at("animation") ]);

        std::map<std::string, std::string> mm;
        std::vector<
          std::map<std::string, std::string> > vmm;
        for (auto& eachCond : to.at("conditions")) {
            for (auto& anyCond : eachCond.items()) {
              mm[ anyCond.key() ] = anyCond.value();
            }
            vmm.push_back(mm);
            mm.clear();
        }
        nA.conditions.push_back(vmm);
      }
    }
  }

  animators[ name ] = std::move(animator);
}

Animator* Resources::getAnimator(std::string name) {
  
  if (animators[ name ] == nullptr) {
    LOG("ERROR: Animator " + name + " not found");
    return nullptr;
  }

  return animators[ name ].get();
}

void Resources::loadMap(std::string name) {

  if (maps[name] != nullptr) {
    LOG("WARNING: map " + name + " already loaded");
    return;
  }

  LOG("load map: " + name);

  std::ifstream f(name);
  if ( ! f) {
    LOG( "ERROR: not found " + name);
    return;
  }

  if ( ! json::accept(f)) {
    LOG("ERROR: not valid json: " + name);
  }

  f.seekg(0);

  json data = json::parse(f);

  std::unique_ptr<Map> map = std::unique_ptr<Map>(
    new Map());

  map->width = data.at("width");
  map->height = data.at("height");
  map->tilewidth = data.at("tilewidth");
  map->tileheight = data.at("tileheight");

  int32_t imap = 0;

  for (auto& layer : data.at("layers")) {
    // drawable (only 1 expected)
    if (layer.at("type") == "tilelayer") {
      for (auto& i : layer.at("data")) {
        map->grid.push_back((int8_t)i);
      }
    }

    // objects can be multiple
    if (layer.at("type") == "objectgroup") {
      for (auto& obj : layer.at("objects")) {

        map->objectsName.push_back(obj.at("name"));
        Rect<int32_t> rec;
        rec.x = obj.at("x");
        rec.y = obj.at("y");
        rec.w = obj.at("width");
        rec.h = obj.at("height");
        map->objectsRect.push_back(rec);

      }
    }
  }

  // For but only 1 expected
  for (auto& tileset : data.at("tilesets")) {

    map->tilesetTextureName = tileset.at("image");
    loadTexture(map->tilesetTextureName);
    Vec<int32_t> vec;
    vec.x = tileset.at("imagewidth");
    vec.y = tileset.at("imageheight");
    map->tilesetSize = vec;
    map->tilesetColumns = tileset.at("columns");

  }

  maps[ name ] = std::move(map);
}

Map* Resources::getMap(std::string name) {
  if (maps[name] == nullptr) {
    LOG("ERROR: map not found with name " + name);
    return nullptr;
  }

  return maps[name].get();
}

void Resources::loadFontConfig(std::string name) {

  if (fontConfigs[name] != nullptr) {
    LOG("WARNING: font config " + name + " already loaded");
    return;
  }

  LOG("load font config: " + name);

  std::unique_ptr<FontConfig> fontConfig =
    std::make_unique<FontConfig>();

  tinyxml2::XMLDocument doc;
  doc.LoadFile(name.c_str());

  assert(doc.ErrorID() == 0);

  tinyxml2::XMLElement* font_e = doc.FirstChildElement("font");
  tinyxml2::XMLElement* info_e = font_e->FirstChildElement("info");
  tinyxml2::XMLElement* common_e = font_e->FirstChildElement("common");
  fontConfig->setLineHeight(common_e->IntAttribute("lineHeight"));
  tinyxml2::XMLElement* page_e = font_e->FirstChildElement("pages")->
    FirstChildElement("page");
  std::string texture_name = page_e->Attribute("file");

  tinyxml2::XMLElement* chars_e = font_e->FirstChildElement("chars");
  tinyxml2::XMLElement* char_e = chars_e->FirstChildElement("char");
  std::vector<uint8_t> ascii;
  std::vector<Rect<int32_t> > texture_area;
  std::vector<Vec<int32_t> > offset_position;
  std::vector<uint32_t> xadvance;
  while (char_e != nullptr) {
    xadvance.push_back(char_e->IntAttribute("xadvance"));
    texture_area.push_back({
      char_e->IntAttribute("x"),
      char_e->IntAttribute("y"),
      char_e->IntAttribute("width"),
      char_e->IntAttribute("height")
      });
    offset_position.push_back({
      char_e->IntAttribute("xoffset"),
      char_e->IntAttribute("yoffset")
      });
    uint32_t id = char_e->IntAttribute("id");
    ascii.push_back(id);

    if (id == 32) {
      fontConfig->setSpaceWidth(char_e->IntAttribute("xadvance"));
    }

    char_e = char_e->NextSiblingElement("char");
  }
  fontConfig->setLetters(
    ascii,
    texture_area,
    offset_position,
    xadvance,
    texture_name);

  tinyxml2::XMLElement* kernings_e = font_e->FirstChildElement("kernings");
  tinyxml2::XMLElement* kerning_e = kernings_e->FirstChildElement("kerning");
  std::vector<uint8_t> firsts;
  std::vector<uint8_t> seconds;
  std::vector<int8_t> amounts;
  while (kerning_e != nullptr) {

    firsts.push_back(kerning_e->IntAttribute("first"));
    seconds.push_back(kerning_e->IntAttribute("second"));
    amounts.push_back(kerning_e->IntAttribute("amount"));

    kerning_e = kerning_e->NextSiblingElement("kerning");
  }
  fontConfig->setKernings(firsts, seconds, amounts);

  fontConfig->validate();

  loadTexture(texture_name);

  fontConfigs[ name ] = std::move(fontConfig);
}

FontConfig* Resources::getFontConfig(std::string name) {
  if (fontConfigs[name] == nullptr) {
    LOG("ERROR: font config not found with name " + name);
    return nullptr;
  }

  return fontConfigs[name].get();
}

void Resources::loadShader(std::string name) {

  if (shaders[name] != nullptr) {
    LOG("WARNING: shader " + name + " already loaded");
    return;
  }

  LOG("load shader: " + name);

  if ( ! sf::Shader::isAvailable()) {
    LOG("ERROR: shader is not available");
    return;
  }

  std::unique_ptr<sf::Shader> shader =
    std::make_unique<sf::Shader>();

  if ( ! shader->loadFromFile(name + ".vert", name + ".frag")) {
    LOG("ERROR: shader not found with name " + name);
    LOG("Look for name + .vert & .frag" + name);
  }
 
  shaders[ name ] = std::move(shader);
}

sf::Shader* Resources::getShader(std::string name) {
  if (shaders[name] == nullptr) {
    LOG("ERROR: shader not found with name " + name);
    return nullptr;
  }

  return shaders[ name ].get();
}

void Resources::loadMusic(std::string name) {
  if (musics[name] != nullptr) {
    LOG("WARNING: music " + name + " already loaded");
    return;
  }

  LOG("load music " + name);

  std::unique_ptr<sf::Music> music = std::make_unique<sf::Music>();
  if ( ! music->openFromFile(name)) {
    LOG("ERROR: music not found in " + name);
  }

  musics[ name ] = std::move(music);
}

sf::Music* Resources::getMusic(std::string name) {
  if (musics[name] == nullptr) {
    LOG("ERROR: music not found in " + name);
  }

  return musics[name].get();
}

void Resources::loadSound(std::string name) {
  if (sounds[name] != nullptr) {
    LOG("WARNING: sound " + name + " already loaded");
    return;
  }

  LOG("load sound " + name);

  std::unique_ptr<sf::SoundBuffer> soundbuffer =
    std::make_unique<sf::SoundBuffer>();
  if ( ! soundbuffer->loadFromFile(name)) {
    LOG("ERROR: soundbuffer not found with name " + name);
  }
  soundbuffers[ name ] = std::move(soundbuffer);

  std::unique_ptr<sf::Sound> sound = std::make_unique<sf::Sound>();
  sound->setBuffer(*soundbuffers[ name ].get());

  sounds[ name ] = std::move(sound);
}

sf::Sound* Resources::getSound(std::string name) {
  if (sounds[name] == nullptr) {
    LOG("ERROR: sound not found in " + name);
  }

  return sounds[ name ].get();
}










