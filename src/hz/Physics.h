#ifndef _PHYSICS_H_
#define _PHYSICS_H_

#include <vector>
#include <stack>
#include <iostream>

#include "sharedtypes.h"
#include "Body.h"

class Body;
class Physics {
public:
	~Physics();

	void update();
  uint32_t generateBody(vf x, vf y, vf width, vf height, bool isDynamic = false);
  bool couldCollide(const int32_t id, vf x, vf y);

  Body* getBody(const uint32_t id);

	static Physics* get();

private:
	Physics();
	static Physics* instancePtr;
	std::stack<uint32_t> availableBody;
	std::vector<Body> bodyPool;

  vf nx, ny, nw, nh;
  Vec<vf> upLeft, upRight, downLeft, downRight;
  vf area1, area2;

	void checkCollideForBodyInX(Body& A, Body& B);
  void checkCollideForBodyInY(Body& A, Body& B);
  bool tryingIsCollidingLeft(Body& A, Body& B);
  bool tryingIsCollidingRight(Body& A, Body& B);
  bool tryingIsCollidingUp(Body& A, Body& B);
  bool tryingIsCollidingDown(Body& A, Body& B);
  bool linesColliding(Vec<vf> a1, Vec<vf> a2, Vec<vf> b1, Vec<vf> b2);
  uint8_t clockWise(Vec<vf> a, Vec<vf> b, Vec<vf> c);
};

#endif
