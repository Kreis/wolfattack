#include "Physics.h"

#define ANTI_CLOCKWISE 1
#define CLOCKWISE 2
#define CONSECUTIVE 0

Physics* Physics::instancePtr = nullptr;
Physics::Physics() {}
Physics::~Physics() {
  if (instancePtr != nullptr) {
    delete instancePtr;
  }
}

void Physics::update() {
	for (uint32_t i = 0; i < bodyPool.size(); i++) {
    
    // ignore dead bodys
		if ( ! bodyPool[ i ].alive) {
      // put it in the pool
      if ( ! bodyPool[ i ].buried) {
        bodyPool[ i ].buried = true;
        availableBody.push(i);
      }
			continue;
		}

    if ( ! bodyPool[ i ].enabled) {
      continue;
    }

    if ( ! bodyPool[ i ].isDynamic) {
      continue;
    }

    bodyPool[ i ].colliding = false;
    bodyPool[ i ].collidingLeft = false;
    bodyPool[ i ].collidingRight = false;
    bodyPool[ i ].collidingDown = false;
    bodyPool[ i ].collidingUp = false;

    for (uint32_t j = 0; j < bodyPool.size(); j++) if (
      i != j &&
      bodyPool[ j ].alive &&
      bodyPool[ j ].enabled &&
      (bodyPool[ i ].type != bodyPool[ j ].type || bodyPool[ i ].type == 0)
    ) {
      checkCollideForBodyInX(bodyPool[ i ], bodyPool[ j ]);
    }

    // update free move
    bodyPool[ i ].x += bodyPool[ i ].tryingMove.x;
    bodyPool[ i ].tryingMove.x = 0;

    for (uint32_t j = 0; j < bodyPool.size(); j++) if (
      i != j &&
      bodyPool[ j ].alive &&
      bodyPool[ j ].enabled &&
      (bodyPool[ i ].type != bodyPool[ j ].type || bodyPool[ i ].type == 0)
    ) {
      checkCollideForBodyInY(bodyPool[ i ], bodyPool[ j ]);
    }

    // update free move
		bodyPool[ i ].y += bodyPool[ i ].tryingMove.y;
		bodyPool[ i ].tryingMove.y = 0;
	}
}

uint32_t Physics::generateBody(vf x, vf y, vf width, vf height, bool isDynamic) {
	if (availableBody.empty()) {
    bodyPool.push_back(Body());
    bodyPool[bodyPool.size() - 1].init(x, y, width, height, isDynamic);
    return static_cast<uint32_t>(bodyPool.size() - 1);
	}
	
  uint32_t availableId = availableBody.top();
  availableBody.pop();

	bodyPool[ availableId ].alive = true;
	bodyPool[ availableId ].enabled = true;
  bodyPool[ availableId ].buried = false;
  bodyPool[ availableId ].x = x;
  bodyPool[ availableId ].y = y;
  bodyPool[ availableId ].width = width;
  bodyPool[ availableId ].height = height;
  bodyPool[ availableId ].isDynamic = isDynamic;
  bodyPool[ availableId ].colliding = false;
  bodyPool[ availableId ].collidingLeft = false;
  bodyPool[ availableId ].collidingRight = false;
  bodyPool[ availableId ].collidingUp = false;
  bodyPool[ availableId ].collidingDown = false;

  return availableId;
}

bool Physics::couldCollide(const int32_t id, vf x, vf y) {
    
  // ignore dead bodys
	if ( ! bodyPool[ id ].alive) {
    return false;
	}

  if ( ! bodyPool[ id ].enabled) {
    return false;
  }

  if ( ! bodyPool[ id ].isDynamic) {
    return false;
  }

  Rect<int32_t> couldBody;
  couldBody.x = (bodyPool[ id ].x + x).get();
  couldBody.y = (bodyPool[ id ].y + y).get();
  couldBody.w = bodyPool[ id ].width.get();
  couldBody.h = bodyPool[ id ].height.get();
  for (uint32_t j = 0; j < bodyPool.size(); j++) if (
    id != j &&
    bodyPool[ j ].alive &&
    bodyPool[ j ].enabled &&
    (bodyPool[ id ].type != bodyPool[ j ].type || bodyPool[ id ].type == 0)
  ) {

    if (couldBody.x >= bodyPool[ j ].x + bodyPool[ j ].width ||
      couldBody.x + couldBody.w <= bodyPool[ j ].x ||
      couldBody.y >= bodyPool[ j ].y + bodyPool[ j ].height ||
      couldBody.y + couldBody.h <= bodyPool[ j ].y) {
      continue;
    }

    return true;
  }
  
  return false;
}

Body* Physics::getBody(const uint32_t id) {
  return &bodyPool[ id ];
}

Physics* Physics::get() {
  if (instancePtr == nullptr) {
    instancePtr = new Physics();
  }

	return instancePtr;
}

void Physics::checkCollideForBodyInX(Body& A, Body& B) {
  if (A.tryingMove.x < 0 && tryingIsCollidingLeft(A, B)) {
    A.tryingMove.x = B.x + B.width - A.x;
    A.colliding = true;
    A.collidingLeft = true;
  } else if (A.tryingMove.x > 0 && tryingIsCollidingRight(A, B)) {
    A.tryingMove.x = B.x - A.x - A.width;
    A.colliding = true;
    A.collidingRight = true;
  }
}

void Physics::checkCollideForBodyInY(Body& A, Body& B) {
  if (A.tryingMove.y > 0 && tryingIsCollidingDown(A, B)) {
    A.tryingMove.y = B.y - A.y - A.height;
    A.colliding = true;
    A.collidingDown = true;
  } else if (A.tryingMove.y < 0 && tryingIsCollidingUp(A, B)) {
    A.tryingMove.y = B.y + B.height - A.y;
    A.colliding = true;
    A.collidingUp = true;
  }
}

bool Physics::tryingIsCollidingLeft(Body& A, Body& B) {
  return A.x + A.tryingMove.x + A.width > B.x && A.x + A.tryingMove.x < B.x + B.width &&
    A.y + A.height > B.y && A.y < B.y + B.height;
}

bool Physics::tryingIsCollidingRight(Body& A, Body& B) {
   return A.x + A.tryingMove.x + A.width > B.x && A.x < B.x + B.width &&
    A.y + A.height > B.y && A.y < B.y + B.height;
}


bool Physics::tryingIsCollidingUp(Body& A, Body& B) {
  return A.x + A.width > B.x && A.x < B.x + B.width &&
    A.y + A.tryingMove.y + A.height > B.y && A.y + A.tryingMove.y < B.y + B.height;
}

bool Physics::tryingIsCollidingDown(Body& A, Body& B) {
  return A.x + A.width > B.x && A.x < B.x + B.width &&
    A.y + A.tryingMove.y + A.height > B.y && A.y < B.y + B.height;
}

bool Physics::linesColliding(Vec<vf> a1, Vec<vf> a2, Vec<vf> b1, Vec<vf> b2) {
  if (clockWise(a1, b1, a2) == clockWise(a1, b2, a2))
    return false;

  if (clockWise(b1, a1, b2) == clockWise(b1, a2, b2))
    return false;

  return true;
}

uint8_t Physics::clockWise(Vec<vf> a, Vec<vf> b, Vec<vf> c) {
  area1 = (a.y - b.y) * (c.x - a.x);
  area2 = (b.x - a.x) * (a.y - c.y);
  if (area1 < area2) {
    return ANTI_CLOCKWISE;
  }

  if (area2 > area1) {
    return CLOCKWISE;
  }

  return CONSECUTIVE;
}

