#ifndef _MAP_H_
#define _MAP_H_

#include <iostream>
#include <vector>

#include <SFML/Graphics.hpp>

#include "sharedtypes.h"

class Map {
friend class Resources;
public:
  ~Map();

  int32_t tilewidth;
  int32_t tileheight;

  int32_t width;
  int32_t height;

  std::vector<int8_t> grid;

  std::vector<Rect<int32_t> > objectsRect;
  std::vector<std::string> objectsName;

  std::string tilesetTextureName;
  Vec<int32_t> tilesetSize;
  int32_t tilesetColumns;
  std::vector<sf::Vertex>* getVecVertex();

private:
  Map();

  std::vector<sf::Vertex> vecVertex;
};


#endif
