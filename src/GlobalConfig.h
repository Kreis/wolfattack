#ifndef _GLOBAL_CONFIG_H_
#define _GLOBAL_CONFIG_H_

#include <memory>
#include <iostream>
#include <vector>

#include "hz/Hz.h"

class GlobalConfig {
public:
  ~GlobalConfig();

  static GlobalConfig* get();

  std::string myusername = "unknown";
  std::vector<int32_t> init_pos;

private:
  GlobalConfig();
  static std::unique_ptr<GlobalConfig> instancePtr;
};

#endif
