#ifndef _PJ_H_
#define _PJ_H_

#include <iostream>

#include "hz/Hz.h"

class Pj {
public:
    Pj();
    ~Pj();

    void init(std::string animator_path, Vec<int32_t> position);
    void update(float deltatime);
    void draw();

    void moveLeft();
    void moveRight();
    void moveDown();
    void moveUp();

    int32_t direction = 0; // 0=before, 1=down, 2=up, 3=right, 4=left
    bool walking = false;

    Animator* animator;

    float x = 0.0;
    float y = 0.0;
    
    bool alive = true;
private:
    std::string animator_path;

    void setDirection();

    float speed = 32.0;


};

#endif