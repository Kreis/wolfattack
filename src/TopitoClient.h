#ifndef TOPITO_CLIENT_H_
#define TOPITO_CLIENT_H_

#include <memory>
#include <vector>

#include "hz/Hz.h"

using json = nlohmann::json;

class TopitoClient {
public:
    ~TopitoClient();

    void init(std::string token);

    void pushID(std::string elem);
    void pushStr256(std::string elem);
    void pushNum32(int32_t elem);
    void send();

    std::vector<json> retrieve();

    static TopitoClient* get();

private:
    TopitoClient();
    static std::unique_ptr<TopitoClient> instancePtr;

    std::vector<uint8_t> message;
};

#endif