#ifndef _LOCAL_PJ_H_
#define _LOCAL_PJ_H_

#include <iostream>

#include "hz/Hz.h"

#include "Pj.h"
#include "TopitoClient.h"
#include "GlobalConfig.h"

class LocalPj {
public:
    LocalPj();
    ~LocalPj();

    void init(std::string animator_path, Vec<int32_t> position);
    void update(float deltatime);
    void draw();

    Pj pj;
};

#endif