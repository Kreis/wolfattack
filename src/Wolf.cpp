#include "Wolf.h"

Wolf::Wolf() {}
Wolf::~Wolf() {}

void Wolf::init() {
    Resources::get()->loadAnimator("data/animators/wolf.json");
    animator = Resources::get()->getAnimator("data/animators/wolf.json");
    animator->runAnimation("running");
}

void Wolf::update(float deltatime) {

    float xdiff = goingx - x;
    float ydiff = goingy - y;

    float my_dist = speed * deltatime;
    float true_dist = sqrt(xdiff * xdiff + ydiff * ydiff);
    if (my_dist >= true_dist) {
        x = goingx;
        y = goingy;
    } else {
        float prop = my_dist / true_dist;
        x += xdiff * prop;
        y += ydiff * prop;
    }

    animator = Resources::get()->getAnimator("data/animators/wolf.json");
    animator->update(deltatime);

    if (attacking != "none") {
        // std::cout << "wolf attacking: " << attacking << std::endl;
    }
}

void Wolf::draw() {
    animator = Resources::get()->getAnimator("data/animators/wolf.json");
    animator->bounds.x = x;
    animator->bounds.y = y;

    if (attacking == "none") {
        animator->setColor({255, 255, 255, 255});
    } else {
        animator->setColor({255, 120, 120, 255});
    }
    animator->draw();
}


void Wolf::handle(std::string key, int32_t val) {
    if (key == "x") {
        x = val;
    }
    if (key == "y") {
        y = val;
    }
    if (key == "goingx") {
        goingx = val;
    }
    if (key == "goingy") {
        goingy = val;
    }
}
